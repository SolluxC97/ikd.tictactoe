#pragma once

#include <iostream>

using namespace std;

class TicTacToe
{

private:

	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:

	void InitBoard()                                   // We'll run this everytime we start a new game
	{
		m_numTurns = 0;
		m_playerTurn = 'X';                            // By default, first player is X
		m_winner = 0;
	}

	void DisplayBoard()
	{
		system("cls");                                 // Clear all-existing console text before we re-write board
		int i = 1;
		for (int j = 0; j < 3; j++)                    // Do this loop a total three times
		{
			cout << m_board[i] << " | ";               // First row, m_board[0],m_board[1],m_board[2]
			i++; ;;                                    // Second row, m_board[3],m_board[4],m_board[5]
			cout << m_board[i] << " | ";               // Third row, m_board[6],m_board[7],m_board[8]
			i++;
			cout << m_board[i] << " |  \n";
			i++;
			cout << "----------- \n";
		}
	}

	bool IsOver()
	{
		if (
			// horizontal
			(m_board[1] == 'X' && m_board[2] == 'X' && m_board[3] == 'X') ||
			(m_board[4] == 'X' && m_board[5] == 'X' && m_board[6] == 'X') ||
		    (m_board[7] == 'X' && m_board[8] == 'X' && m_board[9] == 'X') ||
			// vertical
			(m_board[1] == 'X' && m_board[4] == 'X' && m_board[5] == 'X') ||
			(m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X') ||
			(m_board[3] == 'X' && m_board[6] == 'X' && m_board[9] == 'X') ||
			//diagonal
			(m_board[1] == 'X' && m_board[5] == 'X' && m_board[9] == 'X') ||
			(m_board[3] == 'X' && m_board[5] == 'X' && m_board[7] == 'X')
			)
		{
			m_winner = 'X';
			return true; // Game Is Over. Player X Wins
		}
		else if (
			// horizontal
			(m_board[1] == 'O' && m_board[2] == 'O' && m_board[3] == 'O') ||
			(m_board[4] == 'O' && m_board[5] == 'O' && m_board[6] == 'O') ||
			(m_board[7] == 'O' && m_board[8] == 'O' && m_board[9] == 'O') ||
			// vertical
			(m_board[1] == 'O' && m_board[4] == 'O' && m_board[5] == 'O') ||
			(m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O') ||
			(m_board[3] == 'O' && m_board[6] == 'O' && m_board[9] == 'O') ||
			//diagonal
			(m_board[1] == 'O' && m_board[5] == 'O' && m_board[9] == 'O') ||
			(m_board[3] == 'O' && m_board[5] == 'O' && m_board[7] == 'O')
			)
		{
			m_winner = 'O';
			return true; // Game Is Over. Player O Wins
		}
		else if (m_numTurns >= 9)
		{
			m_winner = 'T'; // T for tie.
			return true;
		}
		else
		{
			return false;
		}
	}

	char GetPlayerTurn()
	{
		if (m_playerTurn == 'X') { m_playerTurn = 'X'; }
		else { m_playerTurn = 'O'; }
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{

		int i = position;
		if (m_board[i] == 'X' || m_board[i] == 'O')      // Is there an X or O in the position we picked?
		{
			cout << "That space has already been taken. \n"; // Print this if there is, and also return false
			return false;
		}
		else
		{
			m_numTurns++;  // Incrment number of turns
			return true;
		}
	}

	void Move(int position)
	{
		int i = position;
		m_board[i] = m_playerTurn;

		if (m_playerTurn == 'X') { m_playerTurn = 'O'; } //Switch to player "O" if player is "X"
		else { m_playerTurn = 'X'; }
	}

	void DisplayResult()
	{
		if (m_winner == 'X') { cout << "Player X Wins! \n"; }
		else if (m_winner == 'O') { cout << "Player O Wins! \n"; }
		else { cout << "The Game is a Tie! \n"; }
	}
}

;