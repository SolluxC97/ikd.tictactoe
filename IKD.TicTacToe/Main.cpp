
#include <iostream>
#include <conio.h>

#include "TicTacToe.h"

using namespace std;

int main()
{
	TicTacToe *pGame = nullptr;

	while (true)
	{
		if (pGame) delete pGame;
		pGame = new TicTacToe;
		pGame->InitBoard();                  // Clear number of turns, and set first player to "X"

		while (!pGame->IsOver())
		{
			pGame->DisplayBoard();

			int position;
			do
			{
				cout << "Player " << pGame->GetPlayerTurn() << ", select a position (1-9) : ";
				cin >> position;
			} 	
			while (!pGame->IsValidMove(position));
			pGame->Move(position);
		}

		//Game Over
		pGame->DisplayBoard();
		pGame->DisplayResult();
        // Prompt to Play Again (Or Quit)
		//char input = ' ';
		char input;
		cout << "Do You Want to Play Again? (y/n) \n";
		cin >> input;
		if (input == 'n' || input == 'N')                       // Did the user input "N" for no?
		{
			return false;
		}
	}
}